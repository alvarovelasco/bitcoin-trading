package org.sonar.challenge.order;

public interface LimitOrderObserver {

	void update(int limit);
	
}
